module.exports = function() {
    this.dbConnections = [];

    this.dbConnections["indunilID"] = {
        host: process.env.EndPoint_rdsIndunilID,
        port: process.env.Port_rdsIndunilID,
        user: process.env.User_rdsIndunilID,
        password: process.env.Password_rdsIndunilID,
        database: "sacheee"
    };
};